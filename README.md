# CS3060_FA2018_R09_ZIMCOSKY

## Reflection Questions

1. Summarize what you learned during this week of class. Note any particular features, concepts, code, or terms that you believe are important. *This should be thorough enough that just by reading your README, someone could learn everything that you did!*
   - In class this week, I learned how to use escript, write a main function in earlang.
   - I also learned about concurrency; I learned the 3 main features (scalability, reliability, and fault tolerance) as well as the 3 primitives (spawning, message recieving, and message sending) of concurrent programming in erlang.
   - I learned how to create a basic loop as shown in the code below.

    ```erlang
   fridge1() ->
    receive
        {From, {store, _Food}} ->
            From ! {self(), ok},
            fridge1();
        {From, {take, _Food}} ->
            %% uh....
            From ! {self(), not_found},
            fridge1();
        terminate ->
            ok
    end.
    ```

   - And how to use linked processes as shown in the code below:

    ```erlang
    fridge2(FoodList) ->
     receive
         {From, {store, Food}} ->
             From ! {self(), ok},
             io:format("~p~n", ["Fridge2:stored"]),
             fridge2([Food|FoodList]);
         {From, {take, Food}} ->
             case lists:member(Food, FoodList) of
                 true ->
                     From ! {self(), {ok, Food}},
                     io:format("~p~n", ["Fridge2:take"]),
                     fridge2(lists:delete(Food, FoodList));
                 false ->
                     From ! {self(), not_found},
                     io:format("~p~n", ["Fridge2:not_found"]),
                     fridge2(FoodList)
             end;
         terminate ->
             ok
     end.
    ```
2. What are your thoughts on concurrency and how it is implemented in Erlang? Is this a foreign concept to you or something you wished you had been exposed to earlier?
   - Concurrency was very foreign to me before workig in Erlang. I see the benefit in being able to preform processes in parellel without them affecting each other unless you purposely pass between these processes. I do wish i was exposed to it earlier, but since I havent I am not sure how to feel about Erlang's implimentation specifically.

3. Which aspect of the work done for this reflection was hardest for you to understand?
   - The hardest aspect for me to understand during this whole section was simply using escript.