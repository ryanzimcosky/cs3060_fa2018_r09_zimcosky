-module(kitchen).
-compile(export_all).

flush()->
    receive
        _ -> flush()
    after 0 -> ok
end.

fridge1() ->
    receive
        {From, {store, _Food}} ->
            From ! {self(), ok},
            fridge1();
        {From, {take, _Food}} ->
            %% uh....
            From ! {self(), not_found},
            fridge1();
        terminate ->
            ok
    end.

fridge2(FoodList) ->
    receive
        {From, {store, Food}} ->
            From ! {self(), ok},
            io:format("~p~n", ["Fridge2:stored"]),
            fridge2([Food|FoodList]);
        {From, {take, Food}} ->
            case lists:member(Food, FoodList) of
                true ->
                    From ! {self(), {ok, Food}},
                    io:format("~p~n", ["Fridge2:take"]),
                    fridge2(lists:delete(Food, FoodList));
                false ->
                    From ! {self(), not_found},
                    io:format("~p~n", ["Fridge2:not_found"]),
                    fridge2(FoodList)
            end;
        terminate ->
            ok
    end.

store(Pid, Food) ->
    Pid ! {self(), {store, Food}},
    receive
        {Pid, Msg} -> Msg,
        io:format("~p~n", [Msg])
    end.

take(Pid, Food) ->
    Pid ! {self(), {take, Food}},
    receive
        {Pid, Msg} -> Msg,
        io:format("~p~n", [Msg])
    end.

start(FoodList) ->
    spawn(kitchen, fridge2, [FoodList]).

main(_) ->
    Pid = spawn(kitchen, fridge2, [[baking_soda]]),
    Pid ! {self(), {store, milk}},
    Pid ! {self(), {store, bacon}},
    Pid ! {self(), {take, bacon}},
    Pid ! {self(), {take, turkey}},
    Pid ! terminate,
    
    Pid2 = spawn(kitchen, fridge2, [[baking_soda]]),
    store(Pid2, water),
    take(Pid2, water),
    take(Pid2, juice),
    Pid2 ! terminate,

    Pid3 = start([rhubarb, dog, hotdog]),
    take(Pid3, dog),
    take(Pid3, dog),
    Pid3 ! terminate,
    flush(),
    io:put_chars(<<>>).




